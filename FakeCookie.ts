import * as moment from 'moment';
import * as _ from 'lodash'

export abstract class FakeCookie{
  static dondeGuardar:any = sessionStorage;
  
  public static set(nombre:string,valor={},expire:string){
    this.dondeGuardar.setItem("fakeCookie"+nombre,JSON.stringify(valor));
    this.dondeGuardar.setItem("fakeCookie"+nombre+"Expire",expire);
  }

  public static get(nombre:string){
    let cookie = this.dondeGuardar.getItem("fakeCookie"+nombre);
    let expire = this.dondeGuardar.getItem("fakeCookie"+nombre+"Expire");
    let unix = moment().unix()
    if(cookie != undefined && unix < parseInt(expire)){
      return JSON.parse(cookie);
    }else{
      return undefined;
    }
  }

  public static getAll(){
    let claves = Object.keys(this.dondeGuardar)
    let datos = _.values(this.dondeGuardar)
    let arrayAux = []
    for(let i = 0; i < claves.length; i++){
      arrayAux.push({key:claves[i],value:datos[i]})
    }
    return arrayAux
  }

  public static setLocalStorage(){
    this.dondeGuardar = localStorage;
  }

  public static setSessionStorage(){
    this.dondeGuardar = sessionStorage;
  }

}